package playground.dataframes

import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types._

object Dataframes extends App {

  // Creating new spark session
  val spark = SparkSession.builder()
    .appName("DataFrame")
    .config("spark.master", "local")
    .getOrCreate()


  // Reading a DF
  val carsDF = spark.read
    .format("json")
    .option("inferSchema", "true")
    .load("src/main/resources/data/cars.json")

  // Showing DF
  carsDF.show()
  carsDF.printSchema()

  // Obtain a schema
  val carsDFSchema = carsDF.schema

  // Get rows
  carsDF.take(10).foreach(println)

  // Spark types
  var longType = LongType

  // Schema
  val carsSchema = StructType(Array(
    StructField("Name", StringType),
    StructField("Miles_per_Gallon", DoubleType),
    StructField("Cylinders", LongType),
    StructField("Displacement", DoubleType),
    StructField("Horsepower", LongType),
    StructField("Weight_in_lbs", LongType),
    StructField("Acceleration", DoubleType),
    StructField("Year", StringType),
    StructField("Origin", StringType)
  ))

  // Read a DF with your schema
  val carsDFWithSchema = spark.read
    .format("json")
    .schema(carsSchema)
    .load("src/main/resources/data/cars.json")

  // Showing a DF
  carsDFWithSchema.show()
  carsDFWithSchema.printSchema()

  // get rows
  carsDFWithSchema.take(10).foreach(println)

  import spark.implicits._

  val smartphones = Seq(
    ("Samsung", "Galaxy S10", "Android", 12),
    ("Apple", "iPhone X", "iOS", 13),
    ("Nokia", "3310", "THE BEST", 0)
  )

  val smartphonesDF = smartphones.toDF("Make", "Model", "Platform", "CameraMegapixels")
  smartphonesDF.show()

}
