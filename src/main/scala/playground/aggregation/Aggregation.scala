package playground.aggregation

import org.apache.spark.sql.{SparkSession, functions}
import org.apache.spark.sql.functions.{stddev, _}

object Aggregation extends App {

  // Creating new Spark session
  val spark = SparkSession.builder()
    .appName(name = "Aggregation and Grouping")
    .config("spark.master", "local")
    .getOrCreate()

  val moviesDF = spark.read
    .option("inferSchema", "true")
    .json("src/main/resources/data/movies.json")

  // Counting
  val genresCountDF = moviesDF.select(count(col("Major_Genre")))
  moviesDF.selectExpr("count(Major_Genre)")

  // Counting distinct
  moviesDF.select(countDistinct(col("Major_Genre"))).show()

  /**
   * 2. Count how many distinct directors we have
   **/

  moviesDF.select(countDistinct(col("Director")).as("Num of directors")).show()

  // Approximate count
  moviesDF.select(approx_count_distinct(col("Major_Genre")))

  // Min and Max
  val minRatingDF = moviesDF.select(min(col("IMDB_Rating")))
  moviesDF.selectExpr("min(IMDB_Rating)")

  // SUM
  moviesDF.select(sum(col("US_Gross")))
  moviesDF.selectExpr("sum(US_Gross)")

  /**
   * 1. Sum up ALL the profits of ALL the movies in the DF
   **/

  import spark.implicits._
  // sum all profits
  val movieProfits = moviesDF.select($"US_Gross" + $"Worldwide_Gross")
  val allProfits = movieProfits.select(sum(col("(US_Gross + Worldwide_Gross)")).as("All Profits"))


  // AVG
  moviesDF.select(avg(col("Rotten_Tomatoes_Rating")))
  moviesDF.selectExpr("avg(Rotten_Tomatoes_Rating)")

  /**
   * 4. Compute the average IMDB rating and the average US gross revenue PER DIRECTOR
   **/

  val avgDirector = moviesDF
    .groupBy(col("Director"))
    .avg("IMDB_Rating", "US_Gross")
    .show()


  /**
   * 3. Show the mean and standard deviation of US gross revenue for the movies
   **/

  moviesDF.select(
    mean(col("US_Gross")),
    stddev(col("US_Gross"))
  ).show()

  moviesDF.select(
    mean(col("Rotten_Tomatoes_Rating")),
    stddev(col("Rotten_Tomatoes_Rating"))
  )



  // Grouping

  val countByGenreDF = moviesDF
    .groupBy(col("Major_Genre"))
    .count()

  val avgRationByGenreDF = moviesDF
    .groupBy(col("Major_Genre"))
    .avg("IMDB_Rating")

  val aggregationByGenreDF = moviesDF
    .groupBy(col("Major_Genre"))
    .agg(
      count("*").as("N.Movies"),
      avg("IMDB_Rating").as("Avg_Rating")
    )
    .orderBy(col("Avg_Rating"))


  /**
   * Exercises
   *
   * 1. Sum up ALL the profits of ALL the movies in the DF
   * 2. Count how many distinct directors we have
   * 3. Show the mean and standard deviation of US gross revenue for the movies
   * 4. Compute the average IMDB rating and the average US gross revenue PER DIRECTOR
   *
   * *
   */

}